package com.timofeev.quizapp.domain.data.local.converter

import androidx.room.TypeConverter
import org.joda.time.LocalDateTime

class LocalDateTimeConverter {

    @TypeConverter
    fun toLocaleDateTime(dateLong: Long): LocalDateTime {
        return LocalDateTime(dateLong)
    }

    @TypeConverter
    fun fromLocaleDateTime(date: LocalDateTime): Long {
        return date.toDate().time
    }
}