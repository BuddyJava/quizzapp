package com.timofeev.quizapp.domain.data.storage

import android.content.res.AssetManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.timofeev.quizapp.entities.network.quiz.QuizResponse
import com.timofeev.quizapp.entities.network.quiz.QuizzesResponse
import io.reactivex.Single
import java.io.InputStreamReader
import javax.inject.Inject

class RawAppData @Inject constructor(
        private val assets: AssetManager,
        private val gson: Gson
) {

    fun getQuizzes(): Single<QuizzesResponse> = fromJsonAsset("app/quizzes.json")
    fun getQuiz(): Single<QuizResponse> = fromJsonAsset("app/quiz.json")

    private inline fun <reified T> fromJsonAsset(pathToAsset: String) = Single.defer {
        assets.open(pathToAsset).use { stream ->
            Single.just<T>(
                    gson.fromJson(
                            InputStreamReader(stream),
                            object : TypeToken<T>() {}.type
                    )
            )
        }
    }

}