package com.timofeev.quizapp.domain.data.local.quiz

import androidx.room.*
import com.timofeev.quizapp.entities.local.quiz.QuizQuestion
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface QuizQuestionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(quizQuestion: QuizQuestion)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg quizQuestions: QuizQuestion)

    @Delete
    fun deleteAll(vararg quizQuestions: QuizQuestion)

    @Query("SELECT * FROM quiz_questions WHERE serverId=:quizQuestionId")
    fun getSingleUnique(quizQuestionId: String): Single<QuizQuestion>

    @Query("SELECT * FROM quiz_questions WHERE quizId=:quizId")
    fun getSingleAll(quizId: String): Single<List<QuizQuestion>>

    @Query("SELECT * FROM quiz_questions WHERE quizId=:quizId")
    fun getFlowableAll(quizId: String): Flowable<List<QuizQuestion>>

}