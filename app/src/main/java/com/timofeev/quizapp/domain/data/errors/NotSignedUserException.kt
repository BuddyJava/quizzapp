package com.timofeev.quizapp.domain.data.errors

class NotSignedUserException(
    override val message: String? = "Function disable for unsigned user"
) :
    Throwable()
