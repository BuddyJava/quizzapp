package com.timofeev.quizapp.domain.data.network.interceptors

import com.timofeev.quizapp.domain.data.errors.ServerError
import com.timofeev.quizapp.entities.network.ErrorResponse
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.Response
import okio.Buffer
import okio.GzipSource
import timber.log.Timber
import java.nio.charset.Charset

class ResponseInterceptor(
    private val gson: Gson
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())


        val code = response.code()
        if (code in 400..500) {

            val headers = response.headers()
            val responseBody = response.body() ?: return response
            val source = responseBody.source()
            source.request(Long.MAX_VALUE) // Buffer the entire body.
            var buffer = source.buffer()

            if ("gzip".equals(headers.get("Content-Encoding")!!, ignoreCase = true)) {
                var gzippedResponseBody: GzipSource? = null
                try {
                    gzippedResponseBody = GzipSource(buffer.clone())
                    buffer = Buffer()
                    buffer.writeAll(gzippedResponseBody)
                } finally {
                    gzippedResponseBody?.close()
                }
            }

            var charset: Charset = UTF8
            val contentType = responseBody.contentType()
            charset = contentType?.charset(UTF8) ?: charset

            var message: String? = null
            var errors: List<String> = emptyList()
            buffer.readString(charset).let {
                try {
                    val body = gson.fromJson<ErrorResponse>(it, ErrorResponse::class.java)
                    message = body.message
                    errors = body.errors
                } catch (e: Exception) {
                    Timber.e(e.message, "Response not parsed")
                }
            }

            throw ServerError(code, message ?: "", errors)
        }

        return response
    }

    companion object {
        private val UTF8 = Charset.forName("UTF-8")
    }
}