package com.timofeev.quizapp.domain.data.local.quiz

import androidx.room.*
import com.timofeev.quizapp.entities.local.quiz.QuizAnswer
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface QuizAnswerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(quizAnswer: QuizAnswer)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg quizAnswers: QuizAnswer)

    @Delete
    fun deleteAll(vararg quizAnswers: QuizAnswer)

    @Query("SELECT * FROM quiz_answers WHERE quizQuestionId=:quizQuestionId")
    fun getSingleAll(quizQuestionId: String): Single<List<QuizAnswer>>

    @Query("SELECT * FROM quiz_answers WHERE serverId IN (:quizAnswerIds)")
    fun getSingleAll(quizAnswerIds: List<String>): Single<List<QuizAnswer>>

    @Query("SELECT * FROM quiz_answers WHERE quizQuestionId=:quizQuestionId")
    fun getFlowableAll(quizQuestionId: String): Flowable<List<QuizAnswer>>

}