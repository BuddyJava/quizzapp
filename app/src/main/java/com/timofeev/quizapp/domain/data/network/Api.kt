package com.timofeev.quizapp.domain.data.network

import com.timofeev.quizapp.entities.network.quiz.QuizResponse
import com.timofeev.quizapp.entities.network.quiz.QuizzesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    //region =================== Quiz ===================

    @GET("quiz/getPopularQuizzes")
    fun getPopularQuizzes(): Single<QuizzesResponse>

    @GET("quiz/getNewestQuizzes")
    fun getNewQuizzes(): Single<QuizzesResponse>

    @GET("quiz/getQuiz")
    fun getQuiz(@Query("quizID", encoded = true) serverId: String): Single<QuizResponse>

    //endregion


}