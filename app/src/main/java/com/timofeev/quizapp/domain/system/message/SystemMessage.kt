package com.timofeev.quizapp.domain.system.message

data class SystemMessage(
    val text: String,
    val type: SystemMessageType = SystemMessageType.ALERT
)
