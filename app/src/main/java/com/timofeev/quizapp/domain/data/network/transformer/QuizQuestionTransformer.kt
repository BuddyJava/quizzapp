package com.timofeev.quizapp.domain.data.network.transformer

import com.timofeev.quizapp.entities.local.quiz.QuizAnswer
import com.timofeev.quizapp.entities.local.quiz.QuizQuestion
import com.timofeev.quizapp.entities.network.quiz.QuizQuestionsResponse
import com.timofeev.quizapp.entities.network.quiz.QuizResponse
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.SingleTransformer
import io.reactivex.functions.BiFunction

class QuizQuestionTransformer(
    private val quizId: String
) : SingleTransformer<QuizResponse, Pair<List<QuizQuestion>, List<QuizAnswer>>> {

    override fun apply(upstream: Single<QuizResponse>): SingleSource<Pair<List<QuizQuestion>, List<QuizAnswer>>> {
        return upstream
            .flatMap { response ->
                convertQuestions(response.questions)
                    .zipWith(
                        convertAnswers(response.questions),
                        BiFunction { questions: List<QuizQuestion>, answers: List<QuizAnswer> ->
                            questions.toList() to answers.toList()
                        }
                    )
            }
    }

    private fun convertQuestions(questions: List<QuizQuestionsResponse>): Single<List<QuizQuestion>> {
        return Observable.fromIterable(questions)
            .map { qa -> QuizQuestion(quizId, qa) }
            .toList()
    }

    private fun convertAnswers(questions: List<QuizQuestionsResponse>): Single<List<QuizAnswer>> {
        return Observable.fromIterable(questions)
            .flatMap { question ->
                Observable.fromIterable(question.answers)
                    .map { answer -> QuizAnswer(question.serverId, answer) }
            }
            .toList()
    }

}