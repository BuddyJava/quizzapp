package com.timofeev.quizapp.domain.system.message

enum class SystemMessageType {
    ALERT,
    TOAST
}