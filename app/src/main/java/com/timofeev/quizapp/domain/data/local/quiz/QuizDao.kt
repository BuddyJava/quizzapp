package com.timofeev.quizapp.domain.data.local.quiz

import androidx.room.*
import com.timofeev.quizapp.entities.local.quiz.Quiz
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface QuizDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(quiz: Quiz)

    @Delete
    fun delete(quiz: Quiz)

    @Query("SELECT * FROM quizzes WHERE serverId=:serverId")
    fun getSingleUnique(serverId: String): Single<Quiz>

    @Query("SELECT * FROM quizzes")
    fun getFlowableAll(): Flowable<List<Quiz>>
}