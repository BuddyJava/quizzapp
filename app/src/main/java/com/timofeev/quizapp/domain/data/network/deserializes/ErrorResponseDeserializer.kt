package com.timofeev.quizapp.domain.data.network.deserializes

import com.timofeev.quizapp.entities.network.ErrorResponse
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class ErrorResponseDeserializer : JsonDeserializer<ErrorResponse> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): ErrorResponse {

        if (json == null || context == null) return ErrorResponse()

        val jsonObject = json.asJsonObject
        val message = jsonObject.get("error").asJsonObject.get("message").asString
        val errors = mutableListOf<String>()
        jsonObject.get("errors").asJsonArray.forEach { element ->
            if (element.isJsonPrimitive) {
                errors.add(element.asString)
            }
        }

        return ErrorResponse(message, errors)
    }
}