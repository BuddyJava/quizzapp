package com.timofeev.quizapp.domain.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.timofeev.quizapp.domain.data.local.quiz.QuizAnswerDao
import com.timofeev.quizapp.domain.data.local.quiz.QuizDao
import com.timofeev.quizapp.domain.data.local.quiz.QuizQuestionDao
import com.timofeev.quizapp.entities.local.quiz.Quiz
import com.timofeev.quizapp.entities.local.quiz.QuizAnswer
import com.timofeev.quizapp.entities.local.quiz.QuizQuestion

@Database(
    entities = [
        Quiz::class, QuizQuestion::class, QuizAnswer::class
    ],
    version = 1
)
abstract class QuizDatabase : RoomDatabase() {

    abstract fun quizDao(): QuizDao
    abstract fun quizQuestionDao(): QuizQuestionDao
    abstract fun quizAnswerDao(): QuizAnswerDao
}