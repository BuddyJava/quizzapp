package com.timofeev.quizapp.domain.system

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.util.DisplayMetrics
import androidx.annotation.ArrayRes
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.res.ResourcesCompat
import javax.inject.Inject

class ResourceManager @Inject constructor(private val context: Context) {
    fun getString(@StringRes stringRes: Int): String {
        return context.resources.getString(stringRes)
    }

    fun getStringArray(@ArrayRes arrayRes: Int): Array<String> {
        return context.resources.getStringArray(arrayRes)
    }

    fun getColor(@ColorRes colorRes: Int, theme: Resources.Theme? = null): Int {
        return ResourcesCompat.getColor(context.resources, colorRes, theme)
    }

    fun getDrawable(@DrawableRes drawableRes: Int, theme: Resources.Theme? = null): Drawable? {
        return ResourcesCompat.getDrawable(context.resources, drawableRes, theme)
    }

    val displayMetrics: DisplayMetrics
        get() = context.resources.displayMetrics

}