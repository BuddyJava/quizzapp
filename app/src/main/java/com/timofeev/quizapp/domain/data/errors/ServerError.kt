package com.timofeev.quizapp.domain.data.errors

class ServerError(
    val code: Int,
    override val message: String,
    val erorrs: List<String> = emptyList()
) : Throwable()