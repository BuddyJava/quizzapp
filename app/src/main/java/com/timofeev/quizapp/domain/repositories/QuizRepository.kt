package com.timofeev.quizapp.domain.repositories

import com.timofeev.quizapp.domain.data.local.QuizDatabase
import com.timofeev.quizapp.domain.data.network.Api
import com.timofeev.quizapp.domain.data.network.transformer.QuizQuestionTransformer
import com.timofeev.quizapp.domain.system.SchedulerProvider
import com.timofeev.quizapp.entities.local.quiz.Quiz
import com.timofeev.quizapp.entities.local.quiz.QuizAnswer
import com.timofeev.quizapp.entities.local.quiz.QuizQuestion
import com.timofeev.quizapp.entities.network.quiz.QuizShortInfoResponse
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class QuizRepository @Inject constructor(
        private val api: Api,
        private val localDb: QuizDatabase,
        private val schedulers: SchedulerProvider
) {

    fun loadQuiz(quizId: String): Completable {
        return api.getQuiz(quizId)
            .map {
                val quiz = Quiz(it)
                localDb.quizDao().insert(quiz)
                it
            }
            .compose(QuizQuestionTransformer(quizId))
            .map { (questions, answers) ->
                localDb.quizQuestionDao().insertAll(*questions.toTypedArray())
                localDb.quizAnswerDao().insertAll(*answers.toTypedArray())
            }
            .ignoreElement()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

    fun getPopularQuizzes(): Single<List<QuizShortInfoResponse>> {
        return api.getPopularQuizzes()
            .map { it.quizzes }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

    fun subscribeViewedQuizzes(): Flowable<List<Quiz>> {
        return localDb.quizDao()
            .getFlowableAll()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

    fun getNewQuizzes(): Single<List<QuizShortInfoResponse>> {
        return api.getNewQuizzes()
            .map { it.quizzes }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

    fun removeQuiz(quizId: String): Completable {
        return localDb.quizDao().getSingleUnique(quizId)
            .doOnSuccess { localDb.quizDao().delete(it) }
            .ignoreElement()
            .andThen(removeQuizQuestions(quizId))
            .andThen(removeQuizAnswers(quizId))
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

    private fun removeQuizQuestions(quizId: String): Completable {
        return localDb.quizQuestionDao().getSingleAll(quizId)
            .doOnSuccess { quizQuestions ->
                localDb.quizQuestionDao().deleteAll(*quizQuestions.toTypedArray())
            }
            .ignoreElement()
    }

    private fun removeQuizAnswers(quizId: String): Completable {
        return localDb.quizAnswerDao().getSingleAll(quizId)
            .doOnSuccess { answers ->
                localDb.quizAnswerDao().deleteAll(*answers.toTypedArray())
            }
            .ignoreElement()
    }

    fun getQuizQuestions(quizId: String): Single<List<QuizQuestion>> {
        return localDb.quizQuestionDao()
            .getSingleAll(quizId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

    fun getQuizAnswers(quizQuestionId: String): Single<List<QuizAnswer>> {
        return localDb.quizAnswerDao()
            .getSingleAll(quizQuestionId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

    fun getQuizAnswers(quizAnswerIds: List<String>): Single<List<QuizAnswer>> {
        return localDb.quizAnswerDao()
            .getSingleAll(quizAnswerIds)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

    fun getQuizQuestion(quizQuestionId: String): Single<QuizQuestion> {
        return localDb.quizQuestionDao()
            .getSingleUnique(quizQuestionId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

    fun getQuiz(quizId: String): Single<Quiz> {
        return localDb.quizDao()
            .getSingleUnique(quizId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

}