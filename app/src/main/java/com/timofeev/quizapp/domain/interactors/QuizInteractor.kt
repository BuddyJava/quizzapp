package com.timofeev.quizapp.domain.interactors

import com.timofeev.quizapp.domain.repositories.QuizRepository
import com.timofeev.quizapp.entities.local.quiz.Quiz
import com.timofeev.quizapp.entities.local.quiz.QuizAnswer
import com.timofeev.quizapp.entities.local.quiz.QuizQuestion
import com.timofeev.quizapp.entities.network.quiz.QuizShortInfo
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class QuizInteractor @Inject constructor(
    private val quizRepository: QuizRepository
) {

    fun subscribeViewedQuizzes(): Flowable<List<QuizShortInfo>> {
        return quizRepository
            .subscribeViewedQuizzes()
            .map { it as List<QuizShortInfo> }
    }

    fun loadQuiz(quizId: String): Completable {
        return quizRepository.loadQuiz(quizId)
    }

    fun getQuizQuestions(quizId: String): Single<List<QuizQuestion>> {
        return quizRepository.getQuizQuestions(quizId)
    }

    fun getQuiz(quizId: String): Single<Quiz> {
        return quizRepository.getQuiz(quizId)
    }

    fun getQuizQuestion(quizQuestionId: String): Single<QuizQuestion> {
        return quizRepository.getQuizQuestion(quizQuestionId)
    }

    fun getQuizAnswers(quizQuestionId: String): Single<List<QuizAnswer>> {
        return quizRepository.getQuizAnswers(quizQuestionId)
    }

    fun getQuizAnswers(quizAnswerIds: List<String>): Single<List<QuizAnswer>> {
        return quizRepository.getQuizAnswers(quizAnswerIds)
    }

    fun loadPopularQuizzes(): Single<List<QuizShortInfo>> {
        return quizRepository.getPopularQuizzes()
            .map { it as List<QuizShortInfo> }
    }

    fun loadNewQuizzes(): Single<List<QuizShortInfo>> {
        return quizRepository.getNewQuizzes()
            .map { it as List<QuizShortInfo> }
    }

    fun removeQuiz(info: QuizShortInfo): Completable {
        return quizRepository.removeQuiz(info.quizId)
    }

}