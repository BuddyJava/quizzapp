package com.timofeev.quizapp.ui.global.list.item.quiz

import android.graphics.PorterDuff
import android.graphics.drawable.Animatable
import com.timofeev.quizapp.R
import com.timofeev.quizapp.entities.local.quiz.QuizAnswer
import com.timofeev.quizapp.utils.getCompatColor
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_quiz_answer.*

class QuizAnswerItem(
    val quizAnswer: QuizAnswer,
    private val selectedId: String
) : Item() {

    override fun getId(): Long {
        return quizAnswer.serverId.toLongOrNull() ?: super.getId()
    }

    override fun getLayout(): Int = R.layout.item_quiz_answer

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.answer.text = quizAnswer.answer

        when {
            selectedId.isNotBlank() && quizAnswer.isCorrect -> {
                viewHolder.wrap.background.setColorFilter(
                    viewHolder.root.context.getCompatColor(R.color.green),
                    PorterDuff.Mode.SRC_ATOP
                )
                viewHolder.answer.setTextColor(viewHolder.root.context.getCompatColor(R.color.white))
                viewHolder.status.setColorFilter(viewHolder.root.context.getCompatColor(R.color.white))
                viewHolder.status.apply {
                    setImageResource(R.drawable.ic_sentiment_satisfied_animated)
                    (drawable as Animatable).start()
                }
            }
            selectedId.isNotBlank() && selectedId == quizAnswer.serverId -> {
                viewHolder.wrap.background.setColorFilter(
                    viewHolder.root.context.getCompatColor(R.color.red),
                    PorterDuff.Mode.SRC_ATOP
                )
                viewHolder.answer.setTextColor(viewHolder.root.context.getCompatColor(R.color.white))
                viewHolder.status.setColorFilter(viewHolder.root.context.getCompatColor(R.color.white))
                viewHolder.status.setImageResource(R.drawable.ic_sentiment_dissatisfied)
            }

            else -> {
                viewHolder.answer.setTextColor(viewHolder.root.context.getCompatColor(R.color.color_text))
                viewHolder.status.setColorFilter(viewHolder.root.context.getCompatColor(R.color.color_icon))
                viewHolder.status.setImageResource(R.drawable.ic_sentiment_neutral)
            }
        }
    }
}