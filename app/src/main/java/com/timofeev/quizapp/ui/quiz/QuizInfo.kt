package com.timofeev.quizapp.ui.quiz

import android.os.Parcelable
import com.timofeev.quizapp.entities.network.quiz.QuizShortInfo
import kotlinx.android.parcel.Parcelize

@Parcelize
data class QuizInfo(
        val id: String,
        val title: String,
        val questionCount: Int,
        val url: String?
) : Parcelable {
    constructor(quiz: QuizShortInfo) : this(
            id = quiz.quizId,
            title = quiz.text,
            questionCount = quiz.questionCount,
            url = quiz.url
    )
}