package com.timofeev.quizapp.ui.quiz

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.timofeev.quizapp.R
import com.timofeev.quizapp.presentation.quiz.QuizQuestionPresenter
import com.timofeev.quizapp.presentation.quiz.QuizQuestionView
import com.timofeev.quizapp.ui.global.BaseFragment
import com.timofeev.quizapp.ui.global.list.item.quiz.QuizAnswerItem
import com.timofeev.quizapp.utils.argument
import com.xwray.groupie.Group
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Section
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_quiz_question.*
import toothpick.Scope
import toothpick.config.Module

class QuizQuestionFragment : BaseFragment(), QuizQuestionView {
    override val layoutRes: Int = R.layout.fragment_quiz_question

    private val quizQuestionInfo by argument<QuizQuestionInfo>(QUESTION_INFO_KEY)

    private val answerSection = Section()
    private var listener: OnQuizAnswerSelected? = null

    private val groupAdapter by lazy {
        GroupAdapter<ViewHolder>().apply {
            add(answerSection)
        }
    }

    override val scopeModuleInstaller = { scope: Scope ->
        scope.installModules(object : Module() {
            init {
                bind(QuizQuestionInfo::class.java).toInstance(quizQuestionInfo)
            }
        })
    }

    @InjectPresenter
    lateinit var presenter: QuizQuestionPresenter

    @ProvidePresenter
    fun createQuizQuestionPresenter(): QuizQuestionPresenter {
        return scope.getInstance(QuizQuestionPresenter::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listener = parentFragment as? OnQuizAnswerSelected
                ?:
                throw NullPointerException("Parent fragment need implement ${OnQuizAnswerSelected::class}")

        answersList.layoutManager = LinearLayoutManager(context)
        answersList.adapter = groupAdapter

        groupAdapter.setOnItemClickListener { item, _ ->
            (item as? QuizAnswerItem)?.quizAnswer?.let { answer ->
                listener?.onQuizAnswerClicked(answer.serverId)
                presenter.onAnswerClicked(answer)
            }
        }
    }

    override fun showResult() {
        listener?.onShowResult()
    }

    override fun showQuestion(question: String) {
        questionView.text =
                HtmlCompat.fromHtml(question, HtmlCompat.FROM_HTML_MODE_COMPACT)
                    .toString()
    }

    override fun showAnswers(list: List<Group>) {
        answerSection.update(list)
    }

    override fun showLeftQuestions(count: Int) {
        listener?.onLeftQuizQuestion(count)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    interface OnQuizAnswerSelected {
        fun onQuizAnswerClicked(selectedAnswerId: String)
        fun onLeftQuizQuestion(leftCount: Int)
        fun onShowResult()
    }

    companion object {
        private const val QUESTION_INFO_KEY = "quiz_question_info_key"

        fun newInstance(questionInfo: QuizQuestionInfo): Fragment {
            return QuizQuestionFragment().apply {
                arguments = bundleOf(QUESTION_INFO_KEY to questionInfo)
            }
        }
    }
}