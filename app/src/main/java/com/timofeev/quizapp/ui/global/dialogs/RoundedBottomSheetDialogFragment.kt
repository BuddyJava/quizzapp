package com.timofeev.quizapp.ui.global.dialogs

import android.app.Dialog
import android.os.Bundle
import com.timofeev.quizapp.R
import com.timofeev.quizapp.ui.global.BaseDialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialog

abstract class RoundedBottomSheetDialogFragment : BaseDialogFragment() {
    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(requireContext(), theme)
}