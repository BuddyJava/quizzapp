package com.timofeev.quizapp.ui.quiz

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.timofeev.quizapp.R
import com.timofeev.quizapp.presentation.quiz.QuizListPresenter
import com.timofeev.quizapp.presentation.quiz.QuizListView
import com.timofeev.quizapp.ui.global.BaseFragment
import com.timofeev.quizapp.ui.global.list.ExpandableHeaderItem
import com.timofeev.quizapp.ui.global.list.item.DividerItem
import com.timofeev.quizapp.ui.global.navigation.ActivityConfiguration
import com.timofeev.quizapp.ui.global.navigation.ConfigurationChanges
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.Group
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.fragment_quiz_list.*
import kotlinx.android.synthetic.main.toolbar.*

class QuizListFragment : BaseFragment(), QuizListView {
    override val layoutRes: Int = R.layout.fragment_quiz_list

    private var viewedExpandableGroup =
            ExpandableGroup(ExpandableHeaderItem(R.string.viewed_group)).apply {
                add(Section().apply {
                    setHideWhenEmpty(true)
                    setFooter(DividerItem())
                })
            }

    private var popularExpandableGroup =
            ExpandableGroup(ExpandableHeaderItem(R.string.popular_group)).apply {
                add(Section().apply {
                    setHideWhenEmpty(true)
                    setFooter(DividerItem())
                })
            }

    private var newExpandableGroup =
            ExpandableGroup(ExpandableHeaderItem(R.string.new_group)).apply {
                add(Section().apply {
                    setHideWhenEmpty(true)
                    setFooter(DividerItem())
                })
            }

    private val groupAdapter by lazy {
        GroupAdapter<ViewHolder>().apply {
            add(viewedExpandableGroup)
            add(popularExpandableGroup)
            add(newExpandableGroup)
        }
    }

    @InjectPresenter
    lateinit var presenter: QuizListPresenter

    @ProvidePresenter
    fun createQuizListPresenter(): QuizListPresenter {
        return scope.getInstance(QuizListPresenter::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as ConfigurationChanges).setActivityConfiguration(
                configuration = ActivityConfiguration(
                        title = getString(R.string.title_quizzes),
                        isHasBackButton = false),
                toolbar = toolbar
        )

        recycleView.layoutManager = LinearLayoutManager(context)
        recycleView.adapter = groupAdapter

        swipeRefresh.setOnRefreshListener { presenter.onSwipeRefresh() }

        groupAdapter.setOnItemClickListener { item, _ ->
            presenter.onItemClicked(item)
        }
    }

    override fun showViewedQuizzes(list: List<Group>) {
        (viewedExpandableGroup.getGroup(1) as Section).update(list)
    }

    override fun showPopularQuizzes(list: List<Group>) {
        (popularExpandableGroup.getGroup(1) as Section).update(list)
    }

    override fun showNewQuizzes(list: List<Group>) {
        (newExpandableGroup.getGroup(1) as Section).update(list)
    }

    override fun hideSwipeRefresh() {
        swipeRefresh.isRefreshing = false
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    companion object {
        fun newInstance(): Fragment {
            return QuizListFragment()
        }
    }
}