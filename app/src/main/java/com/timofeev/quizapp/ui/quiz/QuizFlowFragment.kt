package com.timofeev.quizapp.ui.quiz

import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.timofeev.quizapp.R
import com.timofeev.quizapp.di.DI
import com.timofeev.quizapp.di.modules.FlowNavigationModule
import com.timofeev.quizapp.presentation.quiz.QuizFlowPresenter
import com.timofeev.quizapp.presentation.quiz.QuizFlowView
import com.timofeev.quizapp.ui.global.FlowFragment
import com.timofeev.quizapp.ui.global.navigation.ActivityConfiguration
import com.timofeev.quizapp.ui.global.navigation.ConfigurationChanges
import com.timofeev.quizapp.utils.argument
import com.timofeev.quizapp.utils.changeVisible
import kotlinx.android.synthetic.main.fragment_quiz_flow.*
import kotlinx.android.synthetic.main.toolbar.*
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.Command
import toothpick.Scope
import toothpick.config.Module

class QuizFlowFragment : FlowFragment(), QuizFlowView, QuizQuestionFragment.OnQuizAnswerSelected {
    override val layoutRes: Int = R.layout.fragment_quiz_flow

    private var progressAnimator: ObjectAnimator? = null
    private val quizInfo by argument<QuizInfo>(QUIZ_INFO_KEY)

    override val parentScopeName: String = DI.SERVER_SCOPE

    override val scopeModuleInstaller = { scope: Scope ->
        scope.installModules(FlowNavigationModule(scope.getInstance(Router::class.java)))
        scope.installModules(object : Module() {
            init {
                bind(QuizInfo::class.java).toInstance(quizInfo)
            }
        })
    }

    @InjectPresenter
    lateinit var presenter: QuizFlowPresenter

    @ProvidePresenter
    fun createQuizFlowPresenter(): QuizFlowPresenter {
        return scope.getInstance(QuizFlowPresenter::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        (activity as ConfigurationChanges).setActivityConfiguration(
            configuration = ActivityConfiguration(isHasBackButton = true),
            toolbar = toolbar
        )

        toolbarTitle.changeVisible(true)
        toolbarTitle.text = quizInfo.title

        swipeRefresh.setOnRefreshListener { presenter.onSwipeRefresh() }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.again_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.again_action -> {
                presenter.onAgainClicked()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun enableProgress(enable: Boolean) {
        swipeRefresh.isEnabled = false
    }

    override fun showProgress(visible: Boolean) {
        swipeRefresh.isRefreshing = visible
    }

    override fun showQuizProgress(progress: Int) {
        progressAnimator = ObjectAnimator.ofInt(
            questionProgress,
            "progress",
            questionProgress.progress,
            progress
        )
        progressAnimator?.duration = 300
        progressAnimator?.start()
    }

    override fun showQuizProgressText(message: String) {
        progressText.text = message
    }

    override fun onQuizAnswerClicked(selectedAnswerId: String) {
        presenter.onQuizAnswerClicked(selectedAnswerId)
    }

    override fun onLeftQuizQuestion(leftCount: Int) {
        presenter.onLeftQuizQuestion(leftCount)
    }

    override fun onShowResult() {
        presenter.onShowResult()
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: presenter.onExit()
    }

    override fun setFragmentTransaction(
        command: Command,
        currentFragment: Fragment?,
        nextFragment: Fragment?,
        fragmentTransaction: FragmentTransaction
    ) {

        fragmentTransaction.setCustomAnimations(
            R.anim.animation_open_left,
            R.anim.animation_close_translate,
            R.anim.animation_open_translate,
            R.anim.animation_close_right
        )

    }

    override fun onExit() {
        presenter.onExit()
    }

    companion object {
        private const val QUIZ_INFO_KEY = "quiz_info_key"

        fun newInstance(quizInfo: QuizInfo): Fragment {
            return QuizFlowFragment().apply {
                arguments = bundleOf(QUIZ_INFO_KEY to quizInfo)
            }
        }
    }
}