package com.timofeev.quizapp.ui.quiz

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.timofeev.quizapp.R
import com.timofeev.quizapp.di.DI
import com.timofeev.quizapp.presentation.quiz.QuizResultPresenter
import com.timofeev.quizapp.presentation.quiz.QuizResultView
import com.timofeev.quizapp.presentation.quiz.ResultInfo
import com.timofeev.quizapp.ui.global.BaseFragment
import com.timofeev.quizapp.ui.global.navigation.ActivityConfiguration
import com.timofeev.quizapp.ui.global.navigation.ConfigurationChanges
import com.timofeev.quizapp.utils.argument
import kotlinx.android.synthetic.main.fragment_quiz_result.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Scope
import toothpick.config.Module

class QuizResultFragment : BaseFragment(), QuizResultView {
    override val layoutRes: Int = R.layout.fragment_quiz_result

    private val quizResultInfo by argument<QuizResultInfo>(RESULT_KEY)

    override val parentScopeName: String = DI.SERVER_SCOPE

    override val scopeModuleInstaller = { scope: Scope ->
        scope.installModules(object : Module() {
            init {
                bind(QuizResultInfo::class.java).toInstance(quizResultInfo)
            }
        })
    }

    @InjectPresenter
    lateinit var presenter: QuizResultPresenter

    @ProvidePresenter
    fun createQuizResultPresenter(): QuizResultPresenter {
        return scope.getInstance(QuizResultPresenter::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        (activity as ConfigurationChanges).setActivityConfiguration(
            configuration = ActivityConfiguration(isHasBackButton = false),
            toolbar = toolbar
        )

        doneBtn.setOnClickListener { presenter.onDoneClicked() }
        againBtn.setOnClickListener { presenter.onAgainClicked() }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.share_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.share_action -> {
                presenter.onShareClicked()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    override fun showResultInfo(info: ResultInfo) {
        scoreText.text = info.score
        correctCount.text = info.correctAnswer
        points.text = info.earnedPoints

        star.setImageResource(info.iconRes)
        root.background = ColorDrawable(info.backgroundColor)
        doneBtn.setTextColor(info.backgroundColor)
        (activity as ConfigurationChanges).setActivityConfiguration(
            configuration = ActivityConfiguration(
                isHasBackButton = false,
                colorStatusBar = info.backgroundColor
            ),
            toolbar = toolbar
        )
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    companion object {
        private const val RESULT_KEY = "quiz_result_info_key"

        fun newInstance(quizResultInfo: QuizResultInfo): Fragment {
            return QuizResultFragment().apply {
                arguments = bundleOf(RESULT_KEY to quizResultInfo)
            }
        }
    }
}