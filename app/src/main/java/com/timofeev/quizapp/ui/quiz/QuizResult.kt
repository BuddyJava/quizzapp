package com.timofeev.quizapp.ui.quiz

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class QuizResultInfo(
    val quizId: String,
    val quizQuestionIds: List<String>,
    val quizAnswerIds: List<String>
) : Parcelable