package com.timofeev.quizapp.ui.global

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import java.util.concurrent.TimeUnit

open class BasePresenter<V : MvpView> : MvpPresenter<V>() {
    private val destroyCompositeDisposable = CompositeDisposable()
    private val detachCompositeDisposable = CompositeDisposable()

    override fun detachView(view: V) {
        super.detachView(view)
        detachCompositeDisposable.dispose()
        detachCompositeDisposable.clear()
    }

    override fun onDestroy() {
        destroyCompositeDisposable.dispose()
    }

    protected fun Disposable.disposeOnDestroy() {
        destroyCompositeDisposable.add(this)
    }

    protected fun Disposable.disposeOnDetach() {
        detachCompositeDisposable.add(this)
    }

    protected fun <T> Single<T>.loaderDelay(): Single<T> {
        return this.zipWith(
            Single.timer(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread()),
            BiFunction { data: T, _: Long -> data })
    }
}