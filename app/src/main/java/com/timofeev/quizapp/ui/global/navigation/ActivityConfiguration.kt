package com.timofeev.quizapp.ui.global.navigation

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.timofeev.quizapp.R

data class ActivityConfiguration(
    val title: String? = null,
    @ColorRes val colorMenu: Int = R.color.toolbar_control_normal_color,
    val colorStatusBar: Int? = null,
    @DrawableRes val backButtonImage: Int = R.drawable.ic_arrow_back_black_24dp,
    val isHasBackButton: Boolean = true
)