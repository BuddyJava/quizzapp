package com.timofeev.quizapp.ui

import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.timofeev.quizapp.R
import com.timofeev.quizapp.di.DI
import com.timofeev.quizapp.domain.system.message.SystemMessageNotifier
import com.timofeev.quizapp.domain.system.message.SystemMessageType
import com.timofeev.quizapp.presentation.AppPresenter
import com.timofeev.quizapp.ui.global.BaseActivity
import com.timofeev.quizapp.ui.global.BaseFragment
import com.timofeev.quizapp.ui.quiz.QuizResultFragment
import com.timofeev.quizapp.utils.alertDialogBuilder
import io.reactivex.disposables.Disposable
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import toothpick.Toothpick
import javax.inject.Inject

class AppActivity : BaseActivity(), MvpView {
    override val layoutRes: Int = R.layout.activity_app

    private val currentFragment: BaseFragment?
        get() = supportFragmentManager.findFragmentById(R.id.container) as? BaseFragment

    @Inject
    lateinit var systemMessageNotifier: SystemMessageNotifier

    private var notifierDisposable: Disposable? = null

    @InjectPresenter
    lateinit var presenter: AppPresenter

    @ProvidePresenter
    fun createAppPresenter(): AppPresenter {
        return Toothpick.openScope(DI.SERVER_SCOPE)
            .getInstance(AppPresenter::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)

        savedInstanceState ?: presenter.initFragment()

    }

    override fun onResume() {
        subscribeOnSystemMessages()
        super.onResume()
    }

    override fun onPause() {
        unsubscribeOnSystemMessages()
        super.onPause()
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }

    override val navigator: Navigator =
        object : SupportAppNavigator(this, supportFragmentManager, R.id.container) {
            override fun setupFragmentTransaction(
                command: Command?,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction?
            ) {
                when {
                    isSlideFragment(nextFragment) -> fragmentTransaction?.setCustomAnimations(
                        R.anim.animation_open_left,
                        R.anim.animation_close_translate,
                        R.anim.animation_open_translate,
                        R.anim.animation_close_right
                    )
                    else -> super.setupFragmentTransaction(
                        command,
                        currentFragment,
                        nextFragment,
                        fragmentTransaction
                    )
                }

            }
        }

    private fun isSlideFragment(nextFragment: Fragment?): Boolean {
        return (nextFragment is QuizResultFragment)
    }

    private fun showAlertMessage(message: String) {
        this.alertDialogBuilder {
            setTitle(R.string.oops)
            setMessage(message)
            setPositiveButton(R.string.ok) { dialog, _ -> dialog.dismiss() }
        }.show()
    }

    private fun showToastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun subscribeOnSystemMessages() {
        notifierDisposable = systemMessageNotifier.notifier
            .subscribe { msg ->
                when (msg.type) {
                    SystemMessageType.ALERT -> showAlertMessage(msg.text)
                    SystemMessageType.TOAST -> showToastMessage(msg.text)
                }
            }
    }

    private fun unsubscribeOnSystemMessages() {
        notifierDisposable?.dispose()
    }


}
