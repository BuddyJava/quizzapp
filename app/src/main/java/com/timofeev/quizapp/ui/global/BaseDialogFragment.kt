package com.timofeev.quizapp.ui.global

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timofeev.quizapp.androidx.moxy.MvpAppCompatDialogFragment
import com.timofeev.quizapp.ui.global.dialogs.CustomDialog

abstract class BaseDialogFragment : MvpAppCompatDialogFragment() {
    abstract val layoutRes: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return CustomDialog(context)
    }
}