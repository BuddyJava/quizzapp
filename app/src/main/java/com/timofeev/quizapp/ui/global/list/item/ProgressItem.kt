package com.timofeev.quizapp.ui.global.list.item

import com.timofeev.quizapp.R
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder

class ProgressItem : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {}

    override fun getLayout(): Int = R.layout.item_progress
}