package com.timofeev.quizapp.ui.global

import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.res.ResourcesCompat
import com.timofeev.quizapp.R
import com.timofeev.quizapp.androidx.moxy.MvpAppCompatActivity
import com.timofeev.quizapp.di.DI
import com.timofeev.quizapp.ui.global.navigation.ActivityConfiguration
import com.timofeev.quizapp.ui.global.navigation.ConfigurationChanges
import com.timofeev.quizapp.utils.getCompatColor
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import toothpick.Toothpick
import javax.inject.Inject

abstract class BaseActivity : MvpAppCompatActivity(), ConfigurationChanges {

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    protected abstract val layoutRes: Int
    protected abstract val navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.inject(this, Toothpick.openScope(DI.SERVER_SCOPE))
        super.onCreate(savedInstanceState)

        setContentView(layoutRes)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }


    override fun setActivityConfiguration(configuration: ActivityConfiguration) {
        setActivityConfiguration(configuration, null)
    }

    override fun setActivityConfiguration(configuration: ActivityConfiguration, toolbar: Toolbar?) {
        setSupportActionBar(toolbar)

        with(configuration) {
            window.statusBarColor = colorStatusBar ?:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        this@BaseActivity.getCompatColor(R.color.color_bg_window)
                    } else {
                        this@BaseActivity.getCompatColor(R.color.color_bg_black)
                    }

            supportActionBar ?: return

            supportActionBar?.title = title
            supportActionBar?.setDisplayShowTitleEnabled(title != null)

            supportActionBar?.setDisplayHomeAsUpEnabled(isHasBackButton)
            supportActionBar?.setHomeButtonEnabled(isHasBackButton)

            val homeUpIcon = ResourcesCompat.getDrawable(resources, backButtonImage, null)
            homeUpIcon?.setColorFilter(
                this@BaseActivity.getCompatColor(colorMenu, null),
                PorterDuff.Mode.SRC_IN
            )
            supportActionBar?.setHomeAsUpIndicator(homeUpIcon)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}