package com.timofeev.quizapp.ui.global.navigation

import androidx.appcompat.widget.Toolbar
import com.timofeev.quizapp.ui.global.navigation.ActivityConfiguration

interface ConfigurationChanges {
    fun setActivityConfiguration(configuration: ActivityConfiguration)
    fun setActivityConfiguration(configuration: ActivityConfiguration, toolbar: Toolbar?)
}