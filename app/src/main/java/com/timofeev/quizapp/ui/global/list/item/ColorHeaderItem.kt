package com.timofeev.quizapp.ui.global.list.item

import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import com.timofeev.quizapp.R
import com.timofeev.quizapp.utils.getCompatColor
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_header.*

open class ColorHeaderItem(
    @StringRes titleStringResId: Int,
    @ColorRes private val colorRes: Int? = null
) : HeaderItem(titleStringResId) {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        super.bind(viewHolder, position)
        colorRes?.let {
            viewHolder.title.setTextColor(viewHolder.root.context.getCompatColor(colorRes))
        }

    }
}