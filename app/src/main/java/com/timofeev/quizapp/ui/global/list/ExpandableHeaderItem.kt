package com.timofeev.quizapp.ui.global.list

import android.graphics.drawable.Animatable
import android.view.View
import androidx.annotation.StringRes
import com.timofeev.quizapp.R
import com.timofeev.quizapp.ui.global.list.item.HeaderItem
import com.timofeev.quizapp.utils.getCompatColor
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.ExpandableItem
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_header.*

class ExpandableHeaderItem(
    @StringRes titleStringResId: Int,
    @StringRes subtitleResId: Int? = null
) : HeaderItem(titleStringResId, subtitleResId), ExpandableItem {

    private lateinit var expandableGroup: ExpandableGroup

    override fun bind(viewHolder: ViewHolder, position: Int) {
        super.bind(viewHolder, position)

        // Initial icon state -- not animated.
        viewHolder.root.setOnClickListener {
            expandableGroup.onToggleExpanded()
            bindIcon(viewHolder)
            bindTitle(viewHolder)
        }

        bindTitle(viewHolder)

        viewHolder.icon.apply {
            visibility = View.VISIBLE
            setImageResource(if (expandableGroup.isExpanded) R.drawable.collapse else R.drawable.expand)

        }
    }

    private fun bindIcon(viewHolder: ViewHolder) {
        viewHolder.icon.apply {
            visibility = View.VISIBLE
            setImageResource(if (expandableGroup.isExpanded) R.drawable.collapse_animated else R.drawable.expand_animated)
            (drawable as Animatable).start()
        }
    }

    private fun bindTitle(viewHolder: ViewHolder) {
        val colorRes = if (expandableGroup.isExpanded) R.color.colorAccent else R.color.color_text
        viewHolder.title.setTextColor(viewHolder.root.context.getCompatColor(colorRes))
    }

    override fun setExpandableGroup(onToggleListener: ExpandableGroup) {
        this.expandableGroup = onToggleListener
    }
}