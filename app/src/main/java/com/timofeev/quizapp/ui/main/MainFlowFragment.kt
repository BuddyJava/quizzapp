package com.timofeev.quizapp.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.timofeev.quizapp.R
import com.timofeev.quizapp.Screens
import com.timofeev.quizapp.di.DI
import com.timofeev.quizapp.di.modules.FlowNavigationModule
import com.timofeev.quizapp.presentation.main.MainFlowPresenter
import com.timofeev.quizapp.ui.global.FlowFragment
import com.timofeev.quizapp.utils.setLaunchScreen
import ru.terrakok.cicerone.Router
import toothpick.Scope

class MainFlowFragment : FlowFragment(), MvpView {

    override val parentScopeName: String = DI.SERVER_SCOPE

    override val scopeModuleInstaller = { scope: Scope ->
        scope.installModules(
                FlowNavigationModule(scope.getInstance(Router::class.java))
        )
    }

    @InjectPresenter
    lateinit var presenter: MainFlowPresenter

    @ProvidePresenter
    fun createMainFlowPresenter(): MainFlowPresenter {
        return scope.getInstance(MainFlowPresenter::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (childFragmentManager.fragments.isEmpty()) {
            navigator.setLaunchScreen(Screens.QuizList)
        }
    }

    override fun onExit() {
        presenter.onExit()
    }

    companion object {
        fun newInstance(): Fragment {
            return MainFlowFragment()
        }
    }
}