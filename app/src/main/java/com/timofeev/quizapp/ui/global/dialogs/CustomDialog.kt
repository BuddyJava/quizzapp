package com.timofeev.quizapp.ui.global.dialogs

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatDialog
import androidx.core.content.res.ResourcesCompat
import com.timofeev.quizapp.R

class CustomDialog(
    context: Context?,
    private val isCancelable: Boolean = true,
    private val isHorizontalFill: Boolean = false,
    private val horizontalMargin: Int? = null
) : AppCompatDialog(context, R.style.Theme_Quizzes_Dialog) {

    init {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window?.run {
            // Spread the dialog as large as the screen.
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        }
    }

    override fun setContentView(view: View?) {
        if (view != null) {
            super.setContentView(wrap(view))
        }
    }

    private fun wrap(content: View): View {
        val res = context.resources
        val verticalMargin = res.getDimensionPixelSize(R.dimen.dialog_vertical_margin)
        val horizontalMargin =
            this.horizontalMargin ?: res.getDimensionPixelSize(R.dimen.dialog_horizontal_margin)
        return FrameLayout(context).apply {
            addView(FrameLayout(context)
                .apply {
                    elevation = resources.getDimension(R.dimen.dialog_elevation)
                    clipChildren = true
                    addView(content)
                    background = ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.dialog_bg,
                        context.theme
                    )
                },
                FrameLayout.LayoutParams(
                    if (isHorizontalFill) FrameLayout.LayoutParams.MATCH_PARENT
                    else FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT
                )
                    .apply {
                        setMargins(
                            horizontalMargin,
                            verticalMargin,
                            horizontalMargin,
                            verticalMargin
                        )
                        gravity = Gravity.CENTER
                    })
            val rect = Rect()
            setOnTouchListener { _, event ->
                when (event.action) {
                    // The FrameLayout is technically inside the dialog, but we treat it as outside.
                    MotionEvent.ACTION_DOWN -> {
                        content.getGlobalVisibleRect(rect)
                        if (isCancelable && !rect.contains(event.x.toInt(), event.y.toInt())) {
                            cancel()
                            true
                        } else {
                            false
                        }
                    }
                    else -> {
                        false
                    }
                }
            }
        }
    }
}