package com.timofeev.quizapp.ui.quiz

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class QuizQuestionInfo(
    val quizQuestionIds: List<String>
) : Parcelable