package com.timofeev.quizapp.ui.global.list.item.quiz

import com.timofeev.quizapp.R
import com.timofeev.quizapp.entities.network.quiz.QuizShortInfo
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_swiped_quiz.*
import kotlinx.android.synthetic.main.item_quiz.*

class SwipedQuizItem(
    val info: QuizShortInfo,
    val onQuizClicked: (questionInfo: QuizShortInfo) -> Unit,
    val onRemoveClicked: (questionInfo: QuizShortInfo) -> Unit
) : Item() {

    override fun getId(): Long {
        return info.quizId.toLongOrNull() ?: super.getId()
    }

    override fun getLayout() = R.layout.item_swiped_quiz

    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.topicWrap.setOnClickListener { onQuizClicked(info) }
        viewHolder.remove.setOnClickListener { onRemoveClicked(info) }

        viewHolder.topic.text = info.text
        viewHolder.author.text = viewHolder.root.context.resources.getQuantityString(
            R.plurals.questions,
            info.questionCount,
            info.questionCount
        )
    }
}