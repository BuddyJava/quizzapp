package com.timofeev.quizapp

import android.app.Application
import com.timofeev.quizapp.di.DI
import com.timofeev.quizapp.di.modules.AppModule
import com.timofeev.quizapp.di.modules.ServerModule
import com.facebook.stetho.Stetho
import com.squareup.picasso.Picasso
import net.danlew.android.joda.JodaTimeAndroid
import timber.log.Timber
import toothpick.Toothpick
import toothpick.configuration.Configuration
import java.util.*
import javax.inject.Inject

class App : Application() {

    @Inject
    lateinit var picassoBuild: Picasso

    override fun onCreate() {
        super.onCreate()
        appCode = UUID.randomUUID().toString()

        JodaTimeAndroid.init(this)
        initToothpick()
        initTimber()
        initStetho()
        Picasso.setSingletonInstance(picassoBuild)
    }

    private fun initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction())
        }

        val appScope = Toothpick.openScope(DI.APP_SCOPE)
        appScope.installModules(AppModule(applicationContext))

        val serverScope = Toothpick.openScopes(DI.APP_SCOPE, DI.SERVER_SCOPE)
        serverScope.installModules(ServerModule())
        Toothpick.inject(this, Toothpick.openScope(DI.SERVER_SCOPE))
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }

    companion object {
        lateinit var appCode: String
            private set
    }
}