package com.timofeev.quizapp.utils

import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import java.util.*


fun LocalDateTime.shortTime(): String {
    val nowDate = LocalDateTime.now()

    val period = Period(this, nowDate)
    return when {
        period.years > 0 -> "${period.years} y"
        period.months > 0 -> "${period.months} m"
        period.weeks > 0 -> "${period.weeks} w"
        period.days > 0 -> "${period.days} d"
        period.hours > 0 -> "${period.hours} h"
        period.minutes > 0 -> "${period.minutes} min"
        period.seconds > 0 -> "${period.seconds} s"
        else -> "0s"
    }

}

fun LocalDate.toBirthDayFormat(): String {
    return this.toString(DateTimeFormat.forPattern(BIRTHDAY_FORMAT))
}

fun LocalDateTime.toTransactionFormat(): String {
    return this.toString(TRANSACTION_CREATE_FORMAT, Locale.US)
}

const val BIRTHDAY_FORMAT = "yyyy-MM-dd"
const val TRANSACTION_CREATE_FORMAT = "MMMMM dd, yyyy, HH:mm"