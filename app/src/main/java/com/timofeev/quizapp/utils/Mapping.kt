package com.timofeev.quizapp.utils

import com.timofeev.quizapp.entities.network.quiz.QuizShortInfo
import com.timofeev.quizapp.ui.global.list.item.quiz.QuizShortInfoItem
import com.xwray.groupie.Group
import io.reactivex.Flowable
import io.reactivex.Single

//region =================== Quizzes ===================


fun Single<List<QuizShortInfo>>.mapToQuizShortInfoItems(): Single<List<Group>> {
    return this.flatMap { quizzes ->
        Flowable.fromIterable(quizzes)
                .map { quiz -> QuizShortInfoItem(quiz) }
                .toList()
    }
}

//endregion