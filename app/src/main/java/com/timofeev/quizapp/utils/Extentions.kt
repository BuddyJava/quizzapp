package com.timofeev.quizapp.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import androidx.core.util.PatternsCompat
import com.timofeev.quizapp.R
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Replace

//region =================== Cicerone ===================

fun Navigator.setLaunchScreen(screen: SupportAppScreen) {
    applyCommands(
        arrayOf(
            BackTo(null),
            Replace(screen)
        )
    )
}

//endregion

//region =================== Context ===================

fun Context.alertDialogBuilder(builder: AlertDialog.Builder.() -> Unit): AlertDialog {
    val alertBuilder = AlertDialog.Builder(this, R.style.Theme_Quizzes_AlertDialog)
    alertBuilder.builder()
    return alertBuilder.create()
}

fun Context.shortToast(message: String?) {
    if (TextUtils.isEmpty(message)) return
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.longToast(message: String?) {
    if (TextUtils.isEmpty(message)) return
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Context.getCompatColor(colorRes: Int, theme: Resources.Theme? = null): Int {
    return ResourcesCompat.getColor(this.resources, colorRes, theme)
}

fun Context.openBrowser(uri: Uri, addExtra: (Intent.() -> Unit)? = null) {
    this.startActivity(Intent(Intent.ACTION_VIEW, uri).apply {
        addExtra?.let { this.it() }
    })
}

//endregion

//region =================== View ===================

fun View.changeVisible(visible: Boolean, gone: Boolean = true) {
    val visibility = if (visible) View.VISIBLE else if (gone) View.GONE else View.INVISIBLE
    if (this.visibility != visibility) this.visibility = visibility
}

fun View.showKeyboard() {
    val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    this.requestFocus()
    imm.showSoftInput(this, InputMethodManager.SHOW_FORCED)
}

fun View.hideKeyboard() {
    val imm = this.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}

//endregion

fun Any.objectScopeName(): String = "${javaClass.simpleName}_${hashCode()}"

fun String.isEmailValid() = PatternsCompat.EMAIL_ADDRESS.matcher(this).find()

