package com.timofeev.quizapp.di

object DI {
    const val APP_SCOPE = "app_scope"
    const val SERVER_SCOPE = "server_scope"
}