package com.timofeev.quizapp.di.providers

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.timofeev.quizapp.domain.data.network.deserializes.ErrorResponseDeserializer
import com.timofeev.quizapp.domain.data.network.deserializes.LocalDateTimeDeserializer
import com.timofeev.quizapp.entities.network.ErrorResponse
import org.joda.time.LocalDateTime
import javax.inject.Inject
import javax.inject.Provider

class GsonProvider @Inject constructor() : Provider<Gson> {
    override fun get(): Gson {
        return GsonBuilder()
            .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeDeserializer())
            .registerTypeAdapter(ErrorResponse::class.java, ErrorResponseDeserializer())
            .create()
    }
}