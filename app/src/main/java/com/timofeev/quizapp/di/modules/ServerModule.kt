package com.timofeev.quizapp.di.modules

import com.timofeev.quizapp.di.providers.OkHttpProvider
import com.timofeev.quizapp.di.providers.*
import com.timofeev.quizapp.domain.data.local.QuizDatabase
import com.timofeev.quizapp.domain.data.network.Api
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.timofeev.quizapp.domain.data.storage.RawAppData
import okhttp3.OkHttpClient
import toothpick.config.Module

class ServerModule : Module() {
    init {
        //Network
        bind(Gson::class.java).toProvider(GsonProvider::class.java).providesSingletonInScope()
        bind(OkHttpClient::class.java).toProvider(OkHttpProvider::class.java)
                .providesSingletonInScope()

        bind(Api::class.java).toProvider(ApiProvider::class.java)
                .providesSingletonInScope()
        bind(Picasso::class.java).toProvider(PicassoProvider::class.java)
                .providesSingletonInScope()

        //Local
        bind(QuizDatabase::class.java).toProvider(DatabaseProvider::class.java)
                .providesSingletonInScope()

        bind(RawAppData::class.java).singletonInScope()

    }
}


