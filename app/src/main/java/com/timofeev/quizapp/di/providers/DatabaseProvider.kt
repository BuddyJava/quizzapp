package com.timofeev.quizapp.di.providers

import android.content.Context
import androidx.room.Room
import com.timofeev.quizapp.domain.data.local.QuizDatabase
import javax.inject.Inject
import javax.inject.Provider

class DatabaseProvider @Inject constructor(
    private val context: Context
) : Provider<QuizDatabase> {
    override fun get(): QuizDatabase {
        return Room.databaseBuilder(context, QuizDatabase::class.java, "quiz_db")
            .build()

    }
}