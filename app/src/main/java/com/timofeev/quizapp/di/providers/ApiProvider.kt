package com.timofeev.quizapp.di.providers

import com.timofeev.quizapp.domain.data.network.Api
import com.timofeev.quizapp.domain.data.storage.RawAppData
import com.timofeev.quizapp.entities.network.quiz.QuizResponse
import com.timofeev.quizapp.entities.network.quiz.QuizzesResponse
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Provider


class ApiProvider @Inject constructor(
        private val rawAppData: RawAppData
) : Provider<Api> {

    override fun get(): Api = object : Api {
        override fun getPopularQuizzes(): Single<QuizzesResponse> {
            return rawAppData.getQuizzes()
        }

        override fun getNewQuizzes(): Single<QuizzesResponse> {
            return rawAppData.getQuizzes()
        }

        override fun getQuiz(serverId: String): Single<QuizResponse> {
            return rawAppData.getQuiz()
        }
    }

}