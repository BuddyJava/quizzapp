package com.timofeev.quizapp.di.providers

import android.content.Context
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient
import javax.inject.Inject
import javax.inject.Provider

class PicassoProvider @Inject constructor(
    private val context: Context,
    private val client: OkHttpClient
) : Provider<Picasso> {

    override fun get(): Picasso {
        return Picasso.Builder(context)
            .downloader(OkHttp3Downloader(client))
            .build()
    }
}