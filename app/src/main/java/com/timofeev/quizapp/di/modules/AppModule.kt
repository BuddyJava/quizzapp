package com.timofeev.quizapp.di.modules

import android.content.Context
import android.content.res.AssetManager
import com.timofeev.quizapp.domain.system.AppSchedulers
import com.timofeev.quizapp.domain.system.ResourceManager
import com.timofeev.quizapp.domain.system.SchedulerProvider
import com.timofeev.quizapp.domain.system.message.SystemMessageNotifier
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class AppModule(context: Context) : Module() {

    init {
        //Global
        bind(Context::class.java).toInstance(context)
        bind(ResourceManager::class.java).singletonInScope()
        bind(SchedulerProvider::class.java).toInstance(AppSchedulers())
        bind(SystemMessageNotifier::class.java).toInstance(SystemMessageNotifier())

        bind(AssetManager::class.java).toInstance(context.assets)

        //Navigation
        val cicerone = Cicerone.create(Router())
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

    }

}
