package com.timofeev.quizapp.entities.network

data class ErrorResponse(
    val message: String = "",
    val errors: List<String> = emptyList()
)