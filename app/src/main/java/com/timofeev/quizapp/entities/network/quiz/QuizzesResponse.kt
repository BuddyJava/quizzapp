package com.timofeev.quizapp.entities.network.quiz

import com.google.gson.annotations.SerializedName

data class QuizzesResponse(
    @field:SerializedName("quizzes") val quizzes: List<QuizShortInfoResponse> = emptyList()
)