package com.timofeev.quizapp.entities.local.quiz

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.timofeev.quizapp.domain.data.local.converter.LocalDateTimeConverter
import com.timofeev.quizapp.entities.network.quiz.QuizAnswersResponse
import org.joda.time.LocalDateTime

@Entity(
    tableName = "quiz_answers",
    indices = [Index(
        "serverId",
        unique = true
    )]
)
@TypeConverters(LocalDateTimeConverter::class)
data class QuizAnswer(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val quizQuestionId: String,
    val serverId: String,
    val createdDate: LocalDateTime,
    val answer: String,
    val modifiedDate: LocalDateTime,
    val isCorrect: Boolean,
    val status: String
) {
    constructor(quizQuestionId: String, response: QuizAnswersResponse) : this(
        quizQuestionId = quizQuestionId,
        serverId = response.serverId,
        createdDate = response.createdDate,
        answer = response.answer,
        modifiedDate = response.modifiedDate,
        isCorrect = response.isCorrect == 1,
        status = response.status
    )
}