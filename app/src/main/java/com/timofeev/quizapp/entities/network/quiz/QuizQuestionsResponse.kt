package com.timofeev.quizapp.entities.network.quiz

import com.google.gson.annotations.SerializedName
import org.joda.time.LocalDateTime

data class QuizQuestionsResponse(
    @field:SerializedName("questionID") val serverId: String,
    @field:SerializedName("percentCorrect") val percentCorrect: Double,
    @field:SerializedName("difficulty") val difficulty: Int,
    @field:SerializedName("createdDate") val createdDate: LocalDateTime,
    @field:SerializedName("question") val question: String,
    @field:SerializedName("times_skipped") val timesSkipped: String,
    @field:SerializedName("modifiedDate") val modifiedDate: LocalDateTime,
    @field:SerializedName("answers") val answers: List<QuizAnswersResponse>,
    @field:SerializedName("type") val type: String,
    @field:SerializedName("status") val status: String
)
