package com.timofeev.quizapp.entities.local.quiz

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.timofeev.quizapp.domain.data.local.converter.LocalDateTimeConverter
import com.timofeev.quizapp.entities.network.quiz.QuizResponse
import com.timofeev.quizapp.entities.network.quiz.QuizShortInfo
import org.joda.time.LocalDateTime

@Entity(
    tableName = "quizzes",
    indices = [Index(
        "serverId",
        unique = true
    )]
)
@TypeConverters(LocalDateTimeConverter::class)
data class Quiz(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val serverId: String,
    val title: String,
    val description: String,
    override val questionCount: Int,
    val timesStarted: Int,
    val createdDate: LocalDateTime,
    val modifiedDate: LocalDateTime,
    val sortNumber: Int,
    val imageUrl: String,
    val status: String,
    override val url: String,
    val topicName: String,
    val topicAuthor: String
) : QuizShortInfo {
    constructor(response: QuizResponse) : this(
        serverId = response.serverId,
        title = response.title,
        description = response.description,
        questionCount = response.questionCount,
        timesStarted = response.timesStarted,
        createdDate = response.createdDate,
        modifiedDate = response.modifiedDate,
        sortNumber = response.sortNumber,
        imageUrl = response.imageUrl ?: "",
        status = response.status,
        url = response.url ?: "",
        topicName = response.topics.getOrNull(0)?.topicName ?: "",
        topicAuthor = response.topics.getOrNull(0)?.topicAuthor ?: ""
    )

    override val quizId: String
        get() = serverId
    override val text: String
        get() = title
}