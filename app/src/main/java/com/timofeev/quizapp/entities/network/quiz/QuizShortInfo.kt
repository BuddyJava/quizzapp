package com.timofeev.quizapp.entities.network.quiz

interface QuizShortInfo {
    val quizId: String
    val text: String
    val questionCount: Int
    val url: String?
}