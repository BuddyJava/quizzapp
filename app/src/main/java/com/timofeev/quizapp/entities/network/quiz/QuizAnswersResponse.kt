package com.timofeev.quizapp.entities.network.quiz

import com.google.gson.annotations.SerializedName
import org.joda.time.LocalDateTime

data class QuizAnswersResponse(
    @field:SerializedName("answerID") val serverId: String,
    @field:SerializedName("createdDate") val createdDate: LocalDateTime,
    @field:SerializedName("answer") val answer: String,
    @field:SerializedName("modifiedDate") val modifiedDate: LocalDateTime,
    @field:SerializedName("isCorrect") val isCorrect: Int,
    @field:SerializedName("status") val status: String
)
