package com.timofeev.quizapp.entities.network.quiz

import com.google.gson.annotations.SerializedName

data class SubjectResponse(
    @field:SerializedName("subjectID") val serverId: String,
    @field:SerializedName("name") val name: String,
    @field:SerializedName("visible") val visible: String,
    @field:SerializedName("moreMenu") val moreMenu: String
)
