package com.timofeev.quizapp.entities.network.quiz

import com.google.gson.annotations.SerializedName
import com.timofeev.quizapp.entities.network.TopicResponse
import org.joda.time.LocalDateTime

data class QuizShortInfoResponse(
        @field:SerializedName("quizID") val serverId: String,
        @field:SerializedName("title") val title: String,
        @field:SerializedName("description") val description: String,
        @field:SerializedName("sort_number") val sortNumber: Int,
        @field:SerializedName("questionCount") override val questionCount: Int,

        @field:SerializedName("subject") val subject: SubjectResponse,
        @field:SerializedName("topics") val topics: List<TopicResponse>,

        @field:SerializedName("times_started") val timesStarted: Long,
        @field:SerializedName("createdDate") val createdDate: LocalDateTime,
        @field:SerializedName("modifiedDate") val modifiedDate: LocalDateTime,

        @field:SerializedName("url") override val url: String,
        @field:SerializedName("imageURL") val imageURL: String,
        @field:SerializedName("status") val status: String
) : QuizShortInfo {
    override val quizId: String
        get() = serverId
    override val text: String
        get() = title
}