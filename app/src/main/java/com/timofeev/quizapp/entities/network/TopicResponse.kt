package com.timofeev.quizapp.entities.network

import com.google.gson.annotations.SerializedName

data class TopicResponse(
        @field:SerializedName("topicID") val topicId: String = "",
        @field:SerializedName("topicAuthor") val topicAuthor: String = "",
        @field:SerializedName("topicName") val topicName: String = "",
        @field:SerializedName("topicViews") val topicViews: String = "",
        @field:SerializedName("url") val url: String? = null,
        @field:SerializedName("isFollowed") val isFollowed: Boolean = false
)
