package com.timofeev.quizapp.entities.network.quiz

import com.google.gson.annotations.SerializedName
import com.timofeev.quizapp.entities.network.TopicResponse
import org.joda.time.LocalDateTime

data class QuizResponse(
        @field:SerializedName("quizID") val serverId: String,
        @field:SerializedName("title") val title: String,
        @field:SerializedName("description") val description: String,
        @field:SerializedName("questionCount") val questionCount: Int,
        @field:SerializedName("subject") val subject: SubjectResponse,
        @field:SerializedName("topics") val topics: List<TopicResponse> = emptyList(),
        @field:SerializedName("questions") val questions: List<QuizQuestionsResponse> = emptyList(),
        @field:SerializedName("timesStarted") val timesStarted: Int,
        @field:SerializedName("createdDate") val createdDate: LocalDateTime,
        @field:SerializedName("modifiedDate") val modifiedDate: LocalDateTime,
        @field:SerializedName("sortNumber") val sortNumber: Int,
        @field:SerializedName("url") val url: String? = null,
        @field:SerializedName("imageURL") val imageUrl: String? = null,
        @field:SerializedName("status") val status: String
)
