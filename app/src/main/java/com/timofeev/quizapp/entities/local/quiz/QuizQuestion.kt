package com.timofeev.quizapp.entities.local.quiz

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.timofeev.quizapp.domain.data.local.converter.LocalDateTimeConverter
import com.timofeev.quizapp.entities.network.quiz.QuizQuestionsResponse
import org.joda.time.LocalDateTime

@Entity(
    tableName = "quiz_questions",
    indices = [Index(
        "serverId",
        unique = true
    )]
)
@TypeConverters(LocalDateTimeConverter::class)
data class QuizQuestion(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val quizId: String,
    val serverId: String,
    val percentCorrect: Double,
    val difficulty: Int,
    val createdDate: LocalDateTime,
    val question: String,
    val timesSkipped: String,
    val modifiedDate: LocalDateTime,
    val type: String,
    val status: String
) {
    constructor(quizId: String, response: QuizQuestionsResponse) : this(
        quizId = quizId,
        serverId = response.serverId,
        percentCorrect = response.percentCorrect,
        difficulty = response.difficulty,
        createdDate = response.createdDate,
        question = response.question,
        timesSkipped = response.timesSkipped,
        modifiedDate = response.modifiedDate,
        type = response.type,
        status = response.status
    )
}