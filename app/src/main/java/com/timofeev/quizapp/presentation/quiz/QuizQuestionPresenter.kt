package com.timofeev.quizapp.presentation.quiz

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.timofeev.quizapp.Screens
import com.timofeev.quizapp.domain.interactors.QuizInteractor
import com.timofeev.quizapp.domain.system.flow.FlowRouter
import com.timofeev.quizapp.entities.local.quiz.QuizAnswer
import com.timofeev.quizapp.ui.global.BasePresenter
import com.timofeev.quizapp.ui.global.BaseView
import com.timofeev.quizapp.ui.global.list.item.quiz.QuizAnswerItem
import com.timofeev.quizapp.ui.quiz.QuizQuestionInfo
import com.xwray.groupie.Group
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

interface QuizQuestionView : BaseView {
    fun showQuestion(question: String)
    fun showAnswers(list: List<Group>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showLeftQuestions(count: Int)
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showResult()
}

@InjectViewState
class QuizQuestionPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val quizQuestionInfo: QuizQuestionInfo,
    private val quizInteractor: QuizInteractor
) : BasePresenter<QuizQuestionView>() {

    private var selectedAnswerId: String = ""
    private var answers: List<QuizAnswer> = emptyList()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        val quizQuestionId = quizQuestionInfo.quizQuestionIds.first()
        Timber.d("Quiz question id = $quizQuestionId")

        quizInteractor.getQuizQuestion(quizQuestionId)
            .subscribe(
                { viewState.showQuestion(it.question) },
                { Timber.e(it) }
            )
            .disposeOnDestroy()

        quizInteractor.getQuizAnswers(quizQuestionId)
            .flatMap { answers ->
                this.answers = answers
                Observable.fromIterable(answers)
                    .map { QuizAnswerItem(it, selectedAnswerId) }
                    .toList()
            }
            .subscribe(
                { items -> viewState.showAnswers(items) },
                { Timber.e(it) }
            )
            .disposeOnDestroy()
    }

    fun onAnswerClicked(answer: QuizAnswer) {
        if (selectedAnswerId.isNotBlank()) return

        val newQuizQuestion =
            quizQuestionInfo.quizQuestionIds.subList(1, quizQuestionInfo.quizQuestionIds.size)

        Timber.d("Question left count  = ${newQuizQuestion.size}")

        selectedAnswerId = answer.serverId
        Observable.fromIterable(answers)
            .map { QuizAnswerItem(it, selectedAnswerId) }
            .toList()
            .flatMap { items ->
                viewState.showAnswers(items)
                viewState.showLeftQuestions(newQuizQuestion.size)
                Single.timer(500, TimeUnit.MILLISECONDS)
                    .flatMap {
                        if (newQuizQuestion.isEmpty()) {
                            Single.error(QuestionIsOverException())
                        } else {
                            Single.just(newQuizQuestion)
                        }
                    }
                    .observeOn(AndroidSchedulers.mainThread())
            }
            .subscribe(
                {
                    flowRouter.replaceScreen(
                        Screens.QuizQuestion(QuizQuestionInfo(newQuizQuestion))
                    )
                },
                {
                    when (it) {
                        is QuestionIsOverException -> viewState.showResult()
                        else -> Timber.e(it)
                    }
                }
            )
            .disposeOnDestroy()
    }

    fun onBackPressed() {
        flowRouter.finishFlow()
    }

    class QuestionIsOverException(override val message: String = "Question is over need show result screen") :
        Exception()
}
