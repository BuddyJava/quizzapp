package com.timofeev.quizapp.presentation.main

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpView
import com.timofeev.quizapp.ui.global.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class MainFlowPresenter @Inject constructor(
    private val router: Router
) : BasePresenter<MvpView>() {


    fun onExit() {
        router.exit()
    }

}