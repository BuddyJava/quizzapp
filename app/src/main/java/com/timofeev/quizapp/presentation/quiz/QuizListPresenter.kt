package com.timofeev.quizapp.presentation.quiz

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.timofeev.quizapp.Screens
import com.timofeev.quizapp.domain.interactors.QuizInteractor
import com.timofeev.quizapp.domain.system.flow.FlowRouter
import com.timofeev.quizapp.entities.network.quiz.QuizShortInfo
import com.timofeev.quizapp.ui.global.BasePresenter
import com.timofeev.quizapp.ui.global.BaseView
import com.timofeev.quizapp.ui.global.list.item.quiz.QuizShortInfoItem
import com.timofeev.quizapp.ui.global.list.item.quiz.SwipedQuizItem
import com.timofeev.quizapp.ui.quiz.QuizInfo
import com.timofeev.quizapp.utils.mapToQuizShortInfoItems
import com.xwray.groupie.Group
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import timber.log.Timber
import javax.inject.Inject

interface QuizListView : BaseView {
    fun showViewedQuizzes(list: List<Group>)
    fun showPopularQuizzes(list: List<Group>)
    fun showNewQuizzes(list: List<Group>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun hideSwipeRefresh()
}

@InjectViewState
class QuizListPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val quizInteractor: QuizInteractor
) : BasePresenter<QuizListView>() {

    private var popularQuizzes = emptyList<Group>()
        set(value) {
            field = value
            viewState.showPopularQuizzes(field)
        }

    private var newQuizzes = emptyList<Group>()
        set(value) {
            field = value
            viewState.showNewQuizzes(field)
        }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        quizInteractor.subscribeViewedQuizzes()
            .flatMap { listQuiz ->
                Flowable.fromIterable(listQuiz)
                    .map { quiz ->
                        SwipedQuizItem(quiz,
                            { onQuizClicked(it) },
                            { onRemoveQuiz(it) }
                        )
                    }
                    .toList()
                    .toFlowable()
            }
            .subscribe(
                { viewed -> viewState.showViewedQuizzes(viewed) },
                { Timber.e(it) }
            )
            .disposeOnDestroy()

        quizInteractor.loadPopularQuizzes()
            .mapToQuizShortInfoItems()
            .subscribe({ popularQuizzes = it }, { Timber.e(it) })
            .disposeOnDestroy()

        quizInteractor.loadNewQuizzes()
            .mapToQuizShortInfoItems()
            .subscribe({ newQuizzes = it }, { Timber.e(it) })
            .disposeOnDestroy()
    }

    private fun onRemoveQuiz(info: QuizShortInfo) {
        quizInteractor.removeQuiz(info)
            .subscribe({}, { Timber.e(it) })
            .disposeOnDestroy()
    }

    fun onSwipeRefresh() {
        quizInteractor.loadPopularQuizzes()
            .mapToQuizShortInfoItems()
            .zipWith(
                quizInteractor.loadNewQuizzes()
                    .mapToQuizShortInfoItems(),
                BiFunction { popular: List<Group>, new: List<Group> ->
                    popular to new
                }
            )
            .doAfterTerminate { viewState.hideSwipeRefresh() }
            .subscribe(
                { (popular, new) ->
                    popularQuizzes = popular
                    newQuizzes = new
                },
                { Timber.e(it) }
            )
            .disposeOnDestroy()
    }

    fun onBackPressed() {
        flowRouter.exit()
    }

    fun onItemClicked(item: Item<ViewHolder>) {
        (item as? QuizShortInfoItem)?.info?.let { quiz -> onQuizClicked(quiz) }
    }

    private fun onQuizClicked(quiz: QuizShortInfo) {
        flowRouter.startFlow(Screens.QuizFlow(QuizInfo(quiz)))
    }
}