package com.timofeev.quizapp.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.timofeev.quizapp.Screens
import com.timofeev.quizapp.ui.global.BasePresenter
import com.timofeev.quizapp.ui.global.BaseView
import ru.terrakok.cicerone.Router
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class AppPresenter @Inject constructor(
    private val router: Router
) : BasePresenter<MvpView>() {

    fun initFragment() {
        router.newRootScreen(Screens.MainFlow)
    }

}