package com.timofeev.quizapp.presentation.quiz

import com.arellomobile.mvp.InjectViewState
import com.timofeev.quizapp.R
import com.timofeev.quizapp.Screens
import com.timofeev.quizapp.domain.interactors.QuizInteractor
import com.timofeev.quizapp.domain.system.ResourceManager
import com.timofeev.quizapp.domain.system.flow.FlowRouter
import com.timofeev.quizapp.ui.global.BasePresenter
import com.timofeev.quizapp.ui.global.BaseView
import com.timofeev.quizapp.ui.quiz.QuizInfo
import com.timofeev.quizapp.ui.quiz.QuizQuestionInfo
import com.timofeev.quizapp.ui.quiz.QuizResultInfo
import io.reactivex.Observable
import io.reactivex.Single
import ru.terrakok.cicerone.Router
import timber.log.Timber
import javax.inject.Inject

interface QuizFlowView : BaseView {

    fun enableProgress(enable: Boolean)

    fun showProgress(visible: Boolean)

    fun showQuizProgress(progress: Int)
    fun showQuizProgressText(message: String)
}

@InjectViewState
class QuizFlowPresenter @Inject constructor(
        private val router: Router,
        private val flowRouter: FlowRouter,
        private val quizInfo: QuizInfo,
        private val quizInteractor: QuizInteractor,
        private val resourceManager: ResourceManager
) : BasePresenter<QuizFlowView>() {

    private val quizId: String
        get() = quizInfo.id

    private val questionCount: Int
        get() {
            return if (questionsIds.isEmpty())
                quizInfo.questionCount
            else questionsIds.size
        }

    private val answersIds = mutableListOf<String>()

    private var currentQuestion = 1
        set(value) {
            field = value
            val progress = (currentQuestion.toFloat() / quizInfo.questionCount * 100).toInt()
            viewState.showQuizProgress(progress)
            viewState.showQuizProgressText(
                    String.format(
                            resourceManager.getString(R.string.question_of_percent),
                            currentQuestion,
                            quizInfo.questionCount,
                            progress
                    )
            )
        }

    private var questionsIds: List<String> = emptyList()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        currentQuestion = 0

        viewState.enableProgress(false)

        quizInteractor.getQuizQuestions(quizId)
                .flatMapObservable { questions -> Observable.fromIterable(questions) }
                .map { it.serverId }
                .toList()
                .doOnSuccess {
                    questionsIds = it
                    startQuiz()
                }
                .flatMap {
                    if (questionsIds.isEmpty())
                        loadQuizWithLoaderSingle()
                    else
                        loadQuizSingle()
                }
                .subscribe(
                        {
                            if (questionsIds.isEmpty()) {
                                questionsIds = it
                                startQuiz()
                            }
                        },
                        {
                            Timber.e(it)
                            viewState.enableProgress(true)
                        }
                )
                .disposeOnDestroy()
    }

    private fun startQuiz() {
        if (questionsIds.isNotEmpty()) {
            flowRouter.replaceScreen(Screens.QuizQuestion(QuizQuestionInfo(questionsIds)))
        }
    }

    fun onSwipeRefresh() {
        loadQuizSingle()
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe(
                        {
                            if (questionsIds.isEmpty()) {
                                questionsIds = it
                                startQuiz()
                            }
                            viewState.enableProgress(false)
                        },
                        { Timber.e(it) }
                )
                .disposeOnDestroy()
    }

    private fun loadQuizSingle(): Single<List<String>> {
        return quizInteractor.loadQuiz(quizId)
                .andThen(Single.defer {
                    quizInteractor.getQuizQuestions(quizId)
                            .flatMapObservable { questions -> Observable.fromIterable(questions) }
                            .map { it.serverId }
                            .toList()
                })
    }

    private fun loadQuizWithLoaderSingle(): Single<List<String>> {
        return loadQuizSingle()
                .loaderDelay()
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
    }

    fun onAgainClicked() {
        answersIds.clear()
        currentQuestion = 0
        startQuiz()
    }

    fun onLeftQuizQuestion(leftQuestionCount: Int) {
        currentQuestion = questionCount - leftQuestionCount
    }

    fun onQuizAnswerClicked(selectedAnswerId: String) {
        answersIds.add(selectedAnswerId)
    }

    fun onShowResult() {
        router.replaceScreen(
                Screens.QuizResult(
                        QuizResultInfo(quizId, questionsIds, answersIds)
                )
        )
    }

    fun onExit() {
        router.exit()
    }

}