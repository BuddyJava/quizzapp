package com.timofeev.quizapp.presentation.quiz

import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import com.arellomobile.mvp.InjectViewState
import com.timofeev.quizapp.R
import com.timofeev.quizapp.Screens
import com.timofeev.quizapp.domain.interactors.QuizInteractor
import com.timofeev.quizapp.domain.system.ResourceManager
import com.timofeev.quizapp.ui.global.BasePresenter
import com.timofeev.quizapp.ui.global.BaseView
import com.timofeev.quizapp.ui.quiz.QuizInfo
import com.timofeev.quizapp.ui.quiz.QuizResultInfo
import io.reactivex.Observable
import ru.terrakok.cicerone.Router
import timber.log.Timber
import javax.inject.Inject

data class ResultInfo(
    val score: String,
    val correctAnswer: String,
    val earnedPoints: String,
    @ColorInt val backgroundColor: Int,
    @DrawableRes val iconRes: Int
)

interface QuizResultView : BaseView {
    fun showResultInfo(info: ResultInfo)
}

@InjectViewState
class QuizResultPresenter @Inject constructor(
    private val router: Router,
    private val quizResult: QuizResultInfo,
    private val quizInteractor: QuizInteractor,
    private val resourceManager: ResourceManager
) : BasePresenter<QuizResultView>() {

    init {
        quizInteractor.getQuizAnswers(quizResult.quizAnswerIds)
            .flatMapObservable { Observable.fromIterable(it) }
            .filter { it.isCorrect }
            .toList()
            .map { correctAnswers ->

                getResultInf(correctAnswers.size, quizResult.quizAnswerIds.size)
            }
            .subscribe(
                { viewState.showResultInfo(it) },
                { Timber.e(it) }
            )
            .disposeOnDestroy()
    }

    fun onDoneClicked() {
        router.exit()
    }

    fun onAgainClicked() {
        quizInteractor.getQuiz(quizResult.quizId)
            .subscribe(
                { router.replaceScreen(Screens.QuizFlow(QuizInfo(it))) },
                { Timber.e(it) }
            )
            .disposeOnDestroy()

    }

    fun onBackPressed() {
        router.exit()
    }

    private fun getResultInf(correctAnswers: Int, allAnswers: Int): ResultInfo {
        val correctRatio = (correctAnswers.toFloat() / allAnswers * 100).toInt()

        val score = String.format(resourceManager.getString(R.string.your_score), correctRatio)
        val corrects = String.format(
            resourceManager.getString(R.string.correct_of),
            correctAnswers,
            allAnswers
        )

        val (points: Int, color: Int, imageRes: Int) = when (correctRatio) {
            in 0..9 -> Triple(
                0,
                resourceManager.getColor(R.color.color_zero_percent),
                R.drawable.ic_star_zero_percent
            )

            in 10..39 -> Triple(
                6,
                resourceManager.getColor(R.color.color_twenty_percent),
                R.drawable.ic_star_twenty_percent
            )

            in 40..79 -> Triple(
                12,
                resourceManager.getColor(R.color.color_forty_percent),
                R.drawable.ic_star_forty_percent
            )
            in 80..99 -> Triple(
                24,
                resourceManager.getColor(R.color.color_eighty_percent),
                R.drawable.ic_star_eighty_percent
            )

            100 -> Triple(
                30,
                resourceManager.getColor(R.color.color_hundred_percent),
                R.drawable.ic_star_hungred_percent
            )

            else -> Triple(
                0,
                resourceManager.getColor(R.color.color_zero_percent),
                R.drawable.ic_star_zero_percent
            )
        }

        val earnedPoints =
            if (points == 0) resourceManager.getString(R.string.did_not_earned_points)
            else String.format(resourceManager.getString(R.string.earned_points), points)
        return ResultInfo(score, corrects, earnedPoints, color, imageRes)
    }

    fun onShareClicked() {
        quizInteractor.getQuiz(quizResult.quizId)
            .subscribe(
                { quiz ->
                    router.navigateTo(
                        Screens.Share(
                            String.format(
                                resourceManager.getString(R.string.share_quiz),
                                quiz.topicName,
                                quiz.topicAuthor
                            )
                        )
                    )
                },
                { Timber.e(it) }
            )
            .disposeOnDestroy()
    }
}

