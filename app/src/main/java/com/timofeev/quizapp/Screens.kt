package com.timofeev.quizapp

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import com.timofeev.quizapp.ui.main.MainFlowFragment
import com.timofeev.quizapp.ui.quiz.*
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    object MainFlow : SupportAppScreen() {
        override fun getFragment(): Fragment = MainFlowFragment.newInstance()
    }

    //region =================== Quizzes ===================

    object QuizList : SupportAppScreen() {
        override fun getFragment(): Fragment = QuizListFragment.newInstance()
    }

    class QuizFlow(private val quizInfo: QuizInfo) : SupportAppScreen() {
        override fun getFragment(): Fragment = QuizFlowFragment.newInstance(quizInfo)
    }

    class QuizQuestion(private val quizQuestionInfo: QuizQuestionInfo) : SupportAppScreen() {
        override fun getFragment(): Fragment = QuizQuestionFragment.newInstance(quizQuestionInfo)
    }

    class QuizResult(private val quizResultInfo: QuizResultInfo) : SupportAppScreen() {
        override fun getFragment(): Fragment = QuizResultFragment.newInstance(quizResultInfo)
    }

    //endregion

    //region =================== System ===================

    class Share(private val message: String, private val title: String = "") : SupportAppScreen() {
        override fun getActivityIntent(context: Context?): Intent {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, message)

            return Intent.createChooser(shareIntent, title)
        }
    }

    //endregion
}